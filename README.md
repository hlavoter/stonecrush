# StoneCrush

## A Game in javascript.

Having not absolved ZWA, I didn't want to get into something that I wouldn't be able to realize. My specialization here on CVUT on computer graphics and PC games, and together with the fact that I knew I wouldn't be able to code also backend, it seemed a lot wiser to pick an assignment leaning more towards a game rather than calendar or social network.  
So I chose a simple premise, called it StoneCrush.  

Basic idea of the game was as follows:  
- There is a table of stones in various colors
- Player can switch two neighboring stones if the action results in stones being crushed
- Stones get crushed if they create a block of 3 or more
- If a stone is crushed, one on top of him will fall down and take its place - like gravity
- If there is no stone on top of a crushed one, stage generates new stone with random color
- There is a timer, once it finishes, player can no longer play
- Player gets points for every crushed stone and can later see if their score is placed in a table of best playthroughs   

From technical standpoint I knew I wanted to implement:
- Simple menu
- Page with the game itself
- Options page with difficulty selection and optional profile setup
- ScoreBoard page listing the best playthroughs
- Tutorial page briefly explaining the rules
- And then credits page, but that's more of a joke
- Game rendered through canvas, animated/drawn elements through svg
- Use of HistoryAPI between pages
- Use of LocalStorageAPI to save user profile and records for scoreboard
- Use of FileAPI to load optional user profile picture
- Use of Drag'n'Drop to load optional user profile picture
- Application fully offline
- Sound and visual effects
- Responsive layout that can handle operating on mobile 

## Implementation
### Game
> Files: [game.html](game.html), [game-script.js](js/game-script.js), [Tile.js](js/classes/game/Tile.js), [SVGclock.js](js/classes/game/SVGclock.js), [PauseButton.js](js/classes/game/PauseButton.js) + [Modules.js](js/Modules.js), [UniqueArray.js](js/classes/UniqueArray.js)

I started the whole process with game logic, because that was the one thing I had a clue how to do. It took a while to correctly implement the algorithm for finding stone clusters - or "blocks" as I called them in the sourcecode, but eventually, the game itself was functional. The big mistake was thinking I can master rendering animations in canvas - the one thing I chose the canvas for. I know this game rendering could be done with just html and styling, but where would the fun in that be.
```
    /** Returns true if swap action results in creation of min 3-block unit */
    function willCreateBlocks(tile, other) {
        tile.swapWithTemp(other);
        findBlocks();
        tile.swapWithTemp(other);
        if (stage.crushing.length > 0 && (stage.crushing.indexOf(tile) >= 0 || stage.crushing.indexOf(other) >= 0)) {
            return true;
        }
        return false;
    }
```

So I implemented rendering of my stone table in canvas. Problems started to arise when I got into animation of swapping stones and stones falling. The animation itself was perfect, well timed, smooth - so I think I can give myself pat on the back for understanding this side of javascript. But. What in the end killed animated stones for me was the fact, that I could not make and efficient algorithm for correct falling of the blocks, when some are crushed.  
I tried thinking of it as gravity and pushing every stone down every time interval, I tried finding just the parts that are empty and computing fall length for blocks on top of them. It usually animated well, but in gameplay sense was bugged too much.
I finally settled on "find nearest top block and push it on itself" method for every crushed block and gave up canvas animations.

Score computations were pretty easy, time computations took a while and bit of effort researching.

SVGclock implementation came in after that, I wanted to try creating a very simple shape and depending on remaining time calculating sand ratio in created clock, which I think came out just fine. Because there is no user interaction on the clock, I wanted to make one more element in SVG to try it out - game pause button, styling of which was pretty fun till I figured out the right way, positions and shapes.
### Menu
> Files: [index.html](/index.html), [menu-script.js](js/menu-script.js), [Page.js](js/classes/menu/Page.js), [Router.js](js/classes/menu/Router.js)

I wanted to use the page router from one of the seminars, and I did. But as I went on, it turned out to serve more as an inspiration rather then being reused 1:1.  
I thought it could look very fancy if the inner pages (meaning all pages except for game one) were sliding onto the screen from the right upon selection. This task was surprisingly a lot harder than I imagined, especially with responsive layout in mind.
Options page was the biggest challenge here, because I've never done a form element before and handling the input gave me quite some work for some time. This was also the start of working with APIs.
#### HistoryAPI
Easy in concept, harder to implement than I thought, because of my sliding inner pages. So I had to route back from inner pages to just menu and also react to state being popped from the history queue and reroute the page accordingly.
```
    //reacting to historyAPI going back
        window.addEventListener('popstate', e => {
            this.route(window.location.href);
            const url = new URL(window.location.href);
            const page = url.searchParams.get('page');
            if (page == null) {
                document.body.classList.remove('innerpage-visible');
            } else {
                document.body.classList.add('innerpage-visible');
            }
        });
```
#### Drag'n'Drop & FileAPI
I wanted for the player to be able to load an image as a profile picture and display it during a game on top of the counting score. This went pretty smoothly.
```
    divDrop.addEventListener("drop", (e) => {
            e.preventDefault();
			const files = e.dataTransfer.files;
            this.handleFiles(files);
        });
```
#### LocalStorageAPI
Saving an image turned out to be an uneasy task, but thankfully I did not in the end have to render image data through helper canvas, but could use data blob from filereader I used to upload the image right before. I did not want to get into saving big image files, so this application saves only the most recent player profile fully.  
But score board in local storage is by nickname and those are saved correctly, not just last one.
```
    /** Save form input into local storage (even if its null value) */
    saveToStorage(nickname, catchphrase, difficulty) {
        localStorage.setItem("nickname", nickname);
        localStorage.setItem("catchphrase", catchphrase);
        localStorage.setItem("difficulty", difficulty);
        if (this.image != null) {
            localStorage.setItem("pfp", this.image);
        }
    }
```

### Styling
> Files: [menu.css](css/menu.css), [game.css](css/game.css), [gamesmall.css](css/gamesmall.css)

I went for a quite simplistic look with a lot of transitions and one fun animation on the letter S in the game's name.  
Media queries took most of my time in the final stage of coding, because flex box is quite unintuitive I'd say, especially when it comes to `flex-shirt` and `flex-grow`.  
Trouble I faced here was fixed size rendering of stone table inside the canvas. Because I react to user click and from that calculate on which stone user clicked, changing canvas measurements was fatal for my initial draft. I calculated stone size only once, in the beginning, and thus changing canvas measurements messed up my calculations forward.  
The solution I in the end decided to rely on is event listener on resizing changes and I recompute the whole canvas layout every time.
```
    //resize canvas if window resized
    window.addEventListener("resize", () => {
        computeStoneSize(stage.height, stage.width);
        context.clearRect(0, 0, canvas.width, canvas.height);
        drawStage();
    });
```

For vendor prefixes, I used an online tool we were shown during one of the lectures.   
Game page also includes two modal windows, for which I borrowed the idea from seminars by alerting css code through adding and removing class `start-visible` and `end-visible` to alter the page look for start modal window and end modal window respectively.  

### Audio & Video
I used both, audio clicks are played from javascript everytime user clicks an active button, and also the block crush makes a "boom" sound, played from javascript too.
```
    media.clickSound.pause();
    media.clickSound.currentTime = 0;
    media.clickSound.play();
```
Boom sound is kind of problematic, because block crushes make sound right from the rerouting to the game page, but the rerouting causes most browsers to decline autoplay, thus I wanted to make the start modal window, which requires a click from the user.

And then there is a video of confetti I put on the background with a lower opacity when the timer finishes, it is created and loaded in html, but controlled through API.
```
    const video = document.querySelector('#confettivideo');
    video.play();
```
```
    -webkit-filter: opacity(30%);
    filter: opacity(30%);
```
All used media were free to download and free to use.

## Conclusion
One thing I pity is giving up the canvas animations, but otherwise I am quite satisfied with how the project turned out, especially since my experience with html, css or javascript were down to nonexistant, before this subject.  
Work on this project definitely made me go through the topics much more throughout than studying for semestral tests and I gained quite an amount of new knowledge.

Bonus work implemented in the end includes:
- html graphics svg/canvas
- html media audio/video
- form element
- offline app functionality
- vendor prefixes
- 2D CSS transforms
- media queries
- functional historyAPI
- media control through javascript
- svg control through javascript
