import {Router} from './classes/menu/Router.js';
import {PageNotFound, NoSelectionPage, OptionsPage, TutorialPage, CreditsPage, ScoreboardPage} from './classes/menu/Page.js';

/** Script starts after window is loaded */
window.onload = function() {
    const button = document.createElement('button');
    button.textContent = ">";
    new Router({
        pages: [
            new PageNotFound({key: '404', title: 'Invalid page!!'}),
            new NoSelectionPage({key: 'none', title: 'StoneCrush'}),
            new OptionsPage({key: 'options', title: 'Options'}),
            new TutorialPage({key: 'tutorial', title: 'Tutorial'}),
            new CreditsPage({key: 'credits', title: 'Credits'}),
            new ScoreboardPage({key: 'scoreboard', title: 'ScoreBoard'})
        ],
        defaultPage: 'none'
    });

    
}