import {Tile} from './classes/game/Tile.js';
import {stonetypes} from './Modules.js';
import {UniqueArray} from './classes/UniqueArray.js';
import {SVGclock} from './classes/game/SVGclock.js';
import {PauseButton} from './classes/game/PauseButton.js';
import {media} from './Modules.js';

/** Script starts after window is loaded */
window.onload = function() {
    //show start window
    document.body.classList.add('start-visible');

    //getting canvas
    const canvas = document.getElementById("gameCanvas");
    const context = canvas.getContext("2d");

    //resize canvas if window resized
    window.addEventListener("resize", () => {
        computeStoneSize(stage.height, stage.width);
        context.clearRect(0, 0, canvas.width, canvas.height);
        drawStage();
    });

    //getting score text field
    const scoreTextField = document.getElementById("scoreTextField");
    scoreTextField.textContent = "0";

    //getting time text field
    const timeTextField = document.getElementById("timeTextField");
    timeTextField.textContent = "00:00";

    //event listener user interaction of type click with canvas
    canvas.addEventListener('click', handleClick);


    /** Stage (level) structure */
    const stage = {
        //2d array of tiles
        tiles: new UniqueArray(), 
        //selected tile (or last selected with false flag)
        selected: new Tile(0, 0, '', context, null, false),
        //how many tiles in a column
        height: 10,
        //how many tiles in row
        width: 11,
        //measurement of tile size, both height and width
        stoneSize: 40,
        //gap inside the tile
        gap: 2,
        //array of tiles to be crushed
        crushing: new UniqueArray(),
        //array of tiles to be filled
        empty: new UniqueArray(),
        //max time for level in seconds
        time: 20
    }

    const gameplay = {
        score: 0,
        time: 0,
        control: {state: "init"}
    }

    //difficulty/time has to be set before creating svgClock, because the input value in constructor sets maximum time for ratio computations
    setDifficulty();
    let svgClock = new SVGclock(stage.time * 1000);
    let pauseButton = new PauseButton(gameplay.control);

    /** compute canvas size based on window parameters and device orientation */
    function computeCanvasSize() {
        if (window.innerWidth > window.innerHeight) {
            const size = Math.min(window.innerHeight,window.innerWidth * 7/10);
            canvas.height = window.innerHeight * 8/10;
            canvas.width = size;
            return;
        } else {
            const size = Math.min(window.innerHeight * 8/10, window.innerWidth * 9/10);
            canvas.height = size;
            canvas.width = window.innerWidth * 9/10;
        }
    }

    /** Computes size of a tile based on desired height and width of tile table */
    function computeStoneSize(height, width) {
        computeCanvasSize();
        stage.height = height;
        stage.width = width;
        let sqHeight = Math.floor(canvas.height/height);
        let sqWidth = Math.floor(canvas.width/width);
        let sqSize = Math.min(sqHeight,sqWidth);
        stage.stoneSize = sqSize;
        stage.stoneGap = sqSize * 0.05;
    }

    /** Initializes tile table (random pattern) */
    function generateStage() {
        for (let y = 0; y < stage.height; y++) {
            stage.tiles[y] = [];
            for (let x = 0; x < stage.width; x++) {
                stage.tiles[y][x] = new Tile(x, y, stonetypes.random(stonetypes.length), context, stage);
            }
        }
        findBlocks();
        if (stage.crushing.length > 0) {
            crushBlocks();
        }
    }

    /** Sets difficulty to value saved in local storage,
     *  if it is. If not found - set to normal.
     *  Difficulty settings change time for gameplay.
     *  Time values are in seconds.
     */
    function setDifficulty() {
        const difficulty = localStorage.getItem("difficulty");
        if (difficulty == "easy") {
            stage.time = 120;
        } else if (difficulty == "normal") {
            stage.time = 80;
        } else if (difficulty == "hard") {
            stage.time = 40;
        } else {
            stage.time = 80;
        }
    }

    /** Click handle, user interaction with canvas */
    function handleClick(e) {
        //if game is paused or over, interaction invalid
        if (gameplay.control.state !== "play") {
            return;
        }
        const clickX = e.offsetX;
        const clickY = e.offsetY;
        //getting tile table coords
        const tileX = Math.floor(clickX/stage.stoneSize);
        const tileY = Math.floor(clickY/stage.stoneSize);
        //click outside of tile table
        if (tileX >= stage.width || tileY >= stage.height) {
            stage.selected.deselect();
            return;
        }

        //click is inside the field somewhere
        media.clickSound.pause();
        media.clickSound.currentTime = 0;
        media.clickSound.play();

        //click is inside tile table, we can retrieve it from coords
        const currentTile = stage.tiles[tileY][tileX];
        if (!currentTile.filled) {
            stage.selected.deselect();
            return;
        }
        //some tile already selected, we either deselect or swap
        if (stage.selected.selected == true) {
            stage.selected.deselect();
            //its the same file - ignore
            if (stage.selected == currentTile) {
                return;
            }
            if(stage.selected.isNeighborOf(currentTile) && willCreateBlocks(currentTile, stage.selected)){
                currentTile.swapWith(stage.selected);
                if (stage.crushing.length > 0) {
                    crushBlocks();
                }
            }

        } else { //no tile previously selected, select new one
            currentTile.select();
        }
        
    }

    /** Returns true if swap action results in creation of min 3-block unit */
    function willCreateBlocks(tile, other) {
        tile.swapWithTemp(other);
        findBlocks();
        tile.swapWithTemp(other);
        if (stage.crushing.length > 0 && (stage.crushing.indexOf(tile) >= 0 || stage.crushing.indexOf(other) >= 0)) {
            return true;
        }
        return false;
    }


    /** Finds all 3+ block units in stage and pushes them into stage.crushing array */
    function findBlocks() {
        let blockLength = 1;
        let block = new UniqueArray();
        let lastTileType = null;
        let lastTileFilled = false;
        //vertical blocks
        //across all rows
        for (let y = 0; y < stage.height; y++) {
            blockLength = 1;
            block = new UniqueArray();
            lastTileType = stage.tiles[y][0].type;
            lastTileFilled = stage.tiles[y][0].filled;
            block.push(stage.tiles[y][0]);
            //across all tiles in a row
            for (let x = 1; x < stage.width; x++) {
                //if tile is filled
                if (stage.tiles[y][x].filled) {
                    //and also last one is
                    if (lastTileFilled) {
                        //we found continuous block of tiles
                        if (lastTileType == stage.tiles[y][x].type) {
                            blockLength += 1;
                            block.push(stage.tiles[y][x]);
                        } 
                        //block of tiles ended or row ended
                        if (lastTileType != stage.tiles[y][x].type || x == stage.width - 1) {
                            lastTileType = stage.tiles[y][x].type;
                            //resulting block of tiles is longer than 3 tiles
                            if (blockLength >= 3) {
                                updateScore(blockLength);
                                stage.crushing.pushApplyUnique(block);
                            }
                            block = new UniqueArray();
                            block.push(stage.tiles[y][x]);
                            blockLength = 1;
                        }
                    //this tile is filled, last one is not, we start a new block
                    } else {
                        lastTileFilled = true;
                        lastTileType = stage.tiles[y][x].type;
                        block = new UniqueArray();
                        block.push(stage.tiles[y][x]);
                        blockLength = 1;
                    }
                //tile is empty
                } else {
                    //last one is filled - we check if block does not end here
                    if (lastTileFilled) {
                        //resulting block of tiles is longer than 3 tiles
                        if (blockLength >= 3) {
                            updateScore(blockLength);
                            stage.crushing.pushApplyUnique(block);
                        }
                        block = new UniqueArray();
                    }
                    lastTileFilled = false;
                    continue;
                }
            }
        }

        //horizontal blocks
        //across all columns
        for (let x = 0; x < stage.width; x++) {
            blockLength = 1;
            block = new UniqueArray();
            lastTileType = stage.tiles[0][x].type;
            lastTileFilled = stage.tiles[0][x].filled;
            block.push(stage.tiles[0][x]);
            //across all tiles in a column
            for (let y = 1; y < stage.height; y++) {
                //if tile is filled
                if (stage.tiles[y][x].filled) {
                    //and also last one is
                    if (lastTileFilled) {
                        //we found continuous block of tiles
                        if (lastTileType == stage.tiles[y][x].type) {
                            blockLength += 1;
                            block.push(stage.tiles[y][x]);
                        } 
                        //block of tiles ended or column ended
                        if (lastTileType != stage.tiles[y][x].type || y == stage.height - 1) {
                            lastTileType = stage.tiles[y][x].type;
                            //resulting block of tiles is longer than 3 tiles
                            if (blockLength >= 3) {
                                updateScore(blockLength);
                                stage.crushing.pushApplyUnique(block);
                            }
                            block = new UniqueArray();
                            block.push(stage.tiles[y][x]);
                            blockLength = 1;
                        }
                    //this tile is filled, last one is not, we start a new block
                    }  else {
                        lastTileFilled = true;
                        lastTileType = stage.tiles[y][x].type;
                        block = new UniqueArray();
                        block.push(stage.tiles[y][x]);
                        blockLength = 1;
                    }
                //tile is empty
                } else {
                    //last one is filled - we check if block does not end here
                    if (lastTileFilled) {
                        //resulting block of tiles is longer than 3 tiles
                        if (blockLength >= 3) {
                            updateScore(blockLength);
                            stage.crushing.pushApplyUnique(block);
                        }
                        block = new UniqueArray();
                    }
                    lastTileFilled = false;
                    continue;
                }
            }
        }
    }

    /** Crushes all registered blocks - makes them empty and fills them with upper ones */
    function crushBlocks() {
        //label all blocks in queue as empty
        for (let i = 0; i < stage.crushing.length; i++) {
            stage.crushing[i].filled = false;
            stage.crushing[i].draw();
        }
        //timeout made me use recursion, while cycle bugged it
        //from right to left, bottom to top, make all tiles fill themselves with upper ones
        setTimeout(() => {
            stage.crushing = new UniqueArray();
            for (let y = stage.height - 1; y >= 0; y--) {
                for (let x = stage.width - 1; x >= 0; x--) {
                    if (!stage.tiles[y][x].filled) {
                        stage.tiles[y][x].fill();
                    }
                }
            }
            //play boom sounds
            media.boomSound.pause();
            media.boomSound.currentTime = 0;
            media.boomSound.play();
            //update score after stones are crushed
            scoreTextField.textContent = gameplay.score;
            //check if new blocks emerged
            findBlocks();
            if (stage.crushing.length > 0) {
                //and crush them too
                crushBlocks();
            }
        }, 1000);
        
    }

    /** Adds to score according to crushed block length */
    function updateScore(blockLength) {
        gameplay.score += Math.pow(2, blockLength);
    }

    /** stage printout for debug */
    function printStage() {
        for (let y = 0; y < stage.height; y++) {
            for (let x = 0; x < stage.width; x++) {
                console.log(`\nw: ${x}, h:${y}, color:${stage.tiles[y][x].type}`);
                stage.tiles[y][x].printout();
            }
        }
    }

    /** initial draw of stage */
    function drawStage() {
        for (let y = 0; y < stage.height; y++) {
            for (let x = 0; x < stage.width; x++) {
                stage.tiles[y][x].draw();
            }
        }
    }

    /** Loads values from local storage if they are present
     *  and formats them into end window text
     */
    function setEndWindowText() {
        const textEl = document.querySelector('#endwindowtext');
        const nickname = localStorage.getItem("nickname");
        const catchphrase = localStorage.getItem("catchphrase");
        const difficulty = localStorage.getItem("difficulty");
        //building the string - depends on whether value is in local storage or no
        let string = "Congrats";
        if (nickname) {
            string += ` ${nickname}`;
        }
        string += `!<br>`;
        if (difficulty) {
            string = string.concat(`You finished StoneCrush on ${difficulty} difficulty with a score of ${gameplay.score}!`);
        } else {
            string = string.concat(`You finished StoneCrush on normal difficulty with a score of ${gameplay.score}!`);
        }
        if (catchphrase) {
            string = string.concat(`<br>${catchphrase}!`);
        }
        textEl.innerHTML = string;
    }

    /** Loads image blob from local storage if present and renders */
    function loadPfp() {
        const pfp = localStorage.getItem("pfp");
        if (pfp != null) {
            const imageEl = document.createElement('img');
            imageEl.src = pfp;
            imageEl.width = stage.stoneSize * 5;
            imageEl.height = stage.stoneSize * 5;
            const asideEl = document.querySelector('#imageholder');
            asideEl.appendChild(imageEl);
        }
    }

    /** Computes score and saves it into local storage if needed */
    function setScoreTable() {
        //init table in local storage if not already done
        if (localStorage.getItem('init') != "true") {
            localStorage.setItem('init', "true");
            for (let i = 0; i < 10; i++) {
                localStorage.setItem(i, 0);
                localStorage.setItem(`${i}nick`, "place not taken");
            }
        }
        //compute score with difficulty in mind
        let currentScore = null;
        const difficulty = localStorage.getItem('difficulty');
        if (difficulty == 'easy') {
            currentScore = gameplay.score;
        } else if (difficulty == 'hard') {
            currentScore = gameplay.score * 4;
        } else {
            currentScore = gameplay.score * 2;
        }
        //compute place in table with current score
        let place = 10;
        for (let i = 9; i >= 0; i--) {
            const oldScore = localStorage.getItem(i);
            if (currentScore <= oldScore) {
                break;
            } else {
                place = i;
            }
        }
        //if placed, update table in local storage
        if (place < 10){
            updateScoreTable(currentScore, place);
        }
    }

    /** Update score table in local storage */
    function updateScoreTable(score, place) {
        //switch all places under new scoring
        for (let i = 9; i > place; i--) {
            localStorage.setItem(i, localStorage.getItem(i-1));
            localStorage.setItem(`${i}nick`, localStorage.getItem(`${i-1}nick`));
        }
        //set current scoring to accurate place with set nickname or under anonymous
        localStorage.setItem(place, score);
        let nickname = localStorage.getItem("nickname");
        if (nickname == null) {
            nickname = "anonymous";
        }
        localStorage.setItem(`${place}nick`, nickname);
    }

    /** Printout of scoretable for debug */
    function printScore() {
        for (let i = 0; i < 10; i++) {
            console.log(i+1);
            console.log(localStorage.getItem(i));
            console.log(localStorage.getItem(`${i}nick`));
        }
    }



    //init
    computeStoneSize(stage.height, stage.width);
    generateStage();
    loadPfp();
    drawStage();

    //get start button and wait for user action
    const startbutton = document.querySelector('#startbutton');
    startbutton.addEventListener('click', (e) => {
        gameplay.control.state = "play";
        document.body.classList.remove('start-visible');
        //click sound
        media.clickSound.pause();
        media.clickSound.currentTime = 0;
        media.clickSound.play();
    });

    
    /** Update timer every second */
    let timeLeft = stage.time * 1000;
    let lastTime = new Date().getTime();
    let timerInterval = setInterval(function() {
        if (gameplay.control.state === "play") {
            //current time
            const currentTime = new Date().getTime();  

            //time consumed
            const deltaTime = currentTime - lastTime; 
            timeLeft -= deltaTime;
            lastTime = currentTime;
    
            //computing minutes and seconds
            const minutes = Math.floor((timeLeft % (1000 * 60 * 60)) / (1000 * 60));
            const seconds = String(Math.floor((timeLeft % (1000 * 60)) / 1000)).padStart(2, '0');
            //update display
            timeTextField.textContent = `${minutes}:${seconds}`;
            svgClock.setTime(timeLeft);
        
            //gameover if time is up
            if (timeLeft < 0) {
                clearInterval(timerInterval);
                timeTextField.textContent = "TIME IS UP!!";
                gameplay.control.state = "over";
                //class end-visible added for styling
                document.body.classList.add('end-visible');
                //play victory sound and video
                media.endSound.play();
                const video = document.querySelector('#confettivideo');
                video.play();
                setEndWindowText();
                setScoreTable();
                printScore();
            }
        } else if (gameplay.control.state === "pause") {
            lastTime = new Date().getTime(); 
        }
    }, 1000);

    //go back button
    const backbutton = document.querySelector('#endbackbutton');
    backbutton.addEventListener('click', (e) => {
        //click sound
        media.clickSound.pause();
        media.clickSound.currentTime = 0;
        media.clickSound.play();
        //let click sound play
        setTimeout(() => {
            window.location = "index.html";
        }, 500);
    });
}