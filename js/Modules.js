import { UniqueArray } from "./classes/UniqueArray.js"

/** All stone types */
export const stonetypes = new UniqueArray(

    //warm pastels
    // '#beb9bb',
    // '#6b4e55',
    // '#e8e5c2',
    // '#ff9fac',
    // '#77736f',
    // '#c8a67c'

    //reds
    '#de404c',
    '#fe8366',
    '#f3c9bf',
    '#9b998a',
    '#0c545c',
    '#c8a67c'

    //cold pastels
    // '#886eaa',
    // '#6a938b',
    // '#8cb596',
    // '#f0c1aa',
    // '#faf6d7',
    // '#c8a67c'
);

/** All media constants */
export const media = {
    boomSound: new Audio('media/cinematic-boom.mp3'),
    clickSound: new Audio('media/click.mp3'),
    endSound: new Audio('media/end.mp3')
}
