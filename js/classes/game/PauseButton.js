import {media} from '../../Modules.js';

/** Pause button functionality */
export class PauseButton {
    constructor(control) {
        this.svg = document.querySelector('#svg');
        this.svgns = "http://www.w3.org/2000/svg";
        //getting whole svg group of button content for event handling
        this.pauseButtonEl = document.querySelector('#pausebutton');
        this.pauseButtonEl.addEventListener('click', e=>this.handleClick(e));
        //getting button parts for visibility changes
        this.pauseLEl = document.querySelector('#pauseL');
        this.pauseREl = document.querySelector('#pauseR');
        this.playEl = document.querySelector('#play');
        this.control = control;
    }

    /** Click handling, switch between pause and play if game is not over */
    handleClick(e) {
        media.clickSound.pause();
        media.clickSound.currentTime = 0;
        media.clickSound.play();
        if (this.control.state === "play") {
            this.control.state = "pause";
            this.showPlayButton()
        } else if (this.control.state === "pause") {
            this.control.state = "play";
            this.showPauseButton();
        }    
    }


    /** Play button part hidden, pause button part visible */
    showPauseButton() {
        this.playEl.setAttributeNS(null, 'visibility', 'hidden');
        this.pauseLEl.setAttributeNS(null, 'visibility', 'visible');
        this.pauseREl.setAttributeNS(null, 'visibility', 'visible');
    }

    /** Pause button part hidden, play button part visible */
    showPlayButton() {
        this.pauseLEl.setAttributeNS(null, 'visibility', 'hidden');
        this.pauseREl.setAttributeNS(null, 'visibility', 'hidden');
        this.playEl.setAttributeNS(null, 'visibility', 'visible');
    }
}