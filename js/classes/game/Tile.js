import {stonetypes} from '../../Modules.js';

/** Tile class, represent single stone in canvas */
export class Tile {
    constructor (x, y, type, context, stage, selected = false, filled = true) {
        this.x = x;
        this.y = y;
        this.type = type;
        this.selected = selected;
        this.filled = filled;
        this.solid = true;
        this.context = context;
        this.stage = stage;
    }

    /** Returns true if other tile is neighboring
     *  this tile, otherwise false
     */
    isNeighborOf(other) {
        return ((Math.abs(this.x - other.x) == 1) && this.y == other.y) ||
            ((Math.abs(this.y - other.y) == 1) && this.x == other.x);
    }

    /** Swaps types with other tile, JUST TYPE, for temp testing */
    swapWithTemp(other) {
        this.stage.tiles[this.y][this.x] = other;
        this.stage.tiles[other.y][other.x] = this;
        const tempX = this.x;
        const tempY = this.y;
        this.x = other.x;
        this.y = other.y;
        other.x = tempX;
        other.y = tempY;
    }

    /** Swap with other tile action */
    swapWith(other) {
        this.swapWithTemp(other)
        this.draw();
        other.draw();
    }

    /** Swap with new tile from the top, if there are not any upper ones */
    swapWithTop() {
        this.type = stonetypes.random(stonetypes.length);
        this.filled = true;
        this.draw();
    }

    /** Fill itself with upper filled tile, if empty */
    fill() {
        if (this.filled) {
            return;
        }
        let y = this.y - 1;
        let filled = false;
        while (y >= 0 && !filled) {
            if (this.stage.tiles[y][this.x].filled) {
                this.swapWith(this.stage.tiles[y][this.x]);
                filled = true;
            } else {
                y--;
            }
        }
        if (!filled) {
            this.swapWithTop();
        }
    }

    /** Marks tile as selected, handles redraw */
    select() {
        this.selected = true;
        this.stage.selected = this;
        this.drawSelection();
    }

    /** Marks tile as not selected, handles redraw */
    deselect() {
        this.selected = false;
        this.draw();
    }

    /** Draws tile */
    draw() {
        const context = this.context;
        const stage = this.stage;
        context.clearRect(this.x * stage.stoneSize, this.y * stage.stoneSize, stage.stoneSize, stage.stoneSize);
        if (this.filled) {
            context.fillStyle = this.type;
            context.fillRect(this.x * stage.stoneSize + stage.gap, this.y * stage.stoneSize + stage.gap, stage.stoneSize - 2*stage.gap, stage.stoneSize - 2*stage.gap);
        }
        else { //before being crushed 
            context.fillStyle = this.type;
            context.globalAlpha = 0.5;
            context.fillRect(this.x * stage.stoneSize + stage.gap, this.y * stage.stoneSize + stage.gap, stage.stoneSize - 2*stage.gap, stage.stoneSize - 2*stage.gap);
            context.strokeStyle = 'black';
            context.strokeRect(this.x * stage.stoneSize + stage.gap, this.y * stage.stoneSize + stage.gap, stage.stoneSize - 2*stage.gap, stage.stoneSize - 2*stage.gap);
            context.globalAlpha = 1;
        }         
    }

    /** Draws tile selection */
    drawSelection() {
        const stage = this.stage;
        this.context.strokeStyle = 'red';
        this.context.strokeRect(this.x * stage.stoneSize + stage.gap, this.y * stage.stoneSize + stage.gap, stage.stoneSize - 2*stage.gap, stage.stoneSize - 2*stage.gap);
    }

    printout() {
        console.log(`TILE x:${this.x}, y:${this.y}, type:${this.type}`);
    }
}