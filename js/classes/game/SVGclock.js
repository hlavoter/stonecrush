/** SVG visualization of remaining time */
export class SVGclock {
    //maxTime in milliseconds
    constructor(maxTime) {
        this.svg = document.querySelector('#svg');
        this.svgns = "http://www.w3.org/2000/svg";
        this.timeTopBlock = null;
        this.timeBottomBlock = null;
        this.maxHeight = 55;
        this.createClock(50, 50);
        this.maxTime = maxTime;
        this.borderHeight = 35;
    }

    /** SVG clock drawing init, called only once */
    createClock(x,y) {
        //group for this clock object
        const group = document.createElementNS(this.svgns, 'g');
        group.classList.add('svgclock');
        this.svg.append(group);

        //top sand part - will decrease in height over time
        this.timeTopBlock = document.createElementNS(this.svgns, 'rect');
        this.timeTopBlock.setAttributeNS(null, 'fill', "url(#sandgrad)");
        this.timeTopBlock.setAttributeNS(null, 'width', 79);

        //bottom sand part - will increase in height over time
        this.timeBottomBlock = document.createElementNS(this.svgns, 'rect');
        this.timeBottomBlock.setAttributeNS(null, 'fill', "url(#sandgrad)");
        this.timeBottomBlock.setAttributeNS(null, 'width', 79);

        //cutout in shape of sand clock
        const path = document.createElementNS(this.svgns, 'path');
        path.setAttributeNS(null, 'd', 'M 0 90 L 0 0 L 80 0 L 80 90 L 80 30 L 0 30 L 0 30 a 15 10 90 0 0 80 0 L 80 90 L 0 90 L 80 90 L 80 150 L 80 150 A 15 10 90 0 0 0 150 L 0 90 M 0 150 L 80 150 L 80 180 L 0 180 L 0 150');
        path.setAttributeNS(null, 'fill', "#55656b");

        //glass fill of gradient
        const glass = document.createElementNS(this.svgns, 'rect');
        glass.setAttributeNS(null, 'width', 79);
        glass.setAttributeNS(null, 'height', 179);
        glass.setAttributeNS(null, 'fill', "url(#glassgrad)");

        //appeding in right order is crucial for the shape cutout to work
        group.appendChild(glass);
        group.appendChild(this.timeTopBlock);
        group.appendChild(this.timeBottomBlock);
        group.appendChild(path);
    }

    /** Step-animates the clock - computes ratio of remainingtime/maxtime
     *  and redraws sand parts accordingly.
     *  I tried to animate this part, but got fed up with it after an hour long moment :]]
     */
    setTime(remaining) {
        const ratio = remaining / this.maxTime;
        const height = this.maxHeight * ratio;
        //top sand
        this.timeTopBlock.setAttributeNS(null, 'height', height);
        this.timeTopBlock.setAttributeNS(null, 'y', this.maxHeight - height + this.borderHeight);
        //bottom sand
        this.timeBottomBlock.setAttributeNS(null, 'height', this.maxHeight - height);
        this.timeBottomBlock.setAttributeNS(null, 'y', this.maxHeight + height + this.borderHeight + 5);

        this.lastHeight = height;
    }

}