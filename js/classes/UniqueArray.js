/** My addition to arrays */
export class UniqueArray extends Array {
    /** Random function addition to arrays */
    random(length) {
        return this[Math.floor((Math.random()*length))];
    }

    /** Unique merge with another array addition to arrays */
    pushApplyUnique(array) {
        for (let i = 0; i < array.length; i++) {
            if(this.indexOf(array[i]) === -1) {
                this.push(array[i]);
            }
        }
    }
}