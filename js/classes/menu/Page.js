/** Class frame for inner page */
export class Page {
    constructor({key,title}) {
        this.key = key;
        this.title = title;
        this.pageElement = document.querySelector('#pagecontent');
    }

    render() {
        return '';
    }

    pageShow() {
        this.pageElement.innerHTML = this.render();
        document.title = this.title;
    }

    pageHide() {
        this.pageElement.innerHTML = '';
    }
}

/** Nothing selected from menu, no content visible */
export class NoSelectionPage extends Page {
    constructor (settings) {
        super(settings);
    }   
    
    render() {
        return "";
    }
}

/** Options page for profile and difficulty */
export class OptionsPage extends Page {
    constructor (settings) {
        super(settings);
        this.image = null;
    } 

    pageShow() {
        //render elements
        this.pageElement.innerHTML = this.render();
        //wait for animation to finish, then simulate autofocus
        setTimeout(() => {
            document.querySelector('#inputName').focus();
        }, 500);
        document.title = this.title;
        //prepare div are for Drag'n'Drop of image file
        const divDrop = document.querySelector('#inputDivDrop');
        divDrop.addEventListener("dragover", (e) => {
            e.preventDefault();
        });
        divDrop.addEventListener("drop", (e) => {
            e.preventDefault();
			const files = e.dataTransfer.files;
            this.handleFiles(files);
        });
        //handle image input through FileAPI
        const inputPfp = document.querySelector('#inputPfp');
        inputPfp.onchange = (e) => {
            const files = inputPfp.files;
            if (files.length != 0) {
                this.handleFiles(files);
            }
        }

        //handle form input and save it into local storage
        const form = document.querySelector('#inputForm');
        form.onsubmit = (e) => {
            e.preventDefault();
            const nickname = document.querySelector('#inputName').value;
            const catchphrase = document.querySelector('#inputCatchphrase').value;
            const difficulties = document.getElementsByName("difficulty");
            //finding the selected difficulty through radiobuttons
            let difficulty = "";
            if (difficulties[0].checked) {
                difficulty = "easy";
            } else if (difficulties[1].checked) {
                difficulty = "normal";
            } else {
                difficulty = "hard";
            }
            this.saveToStorage(nickname, catchphrase, difficulty);
        }
    }

    /** Save form input into local storage (even if its null value) */
    saveToStorage(nickname, catchphrase, difficulty) {
        localStorage.setItem("nickname", nickname);
        localStorage.setItem("catchphrase", catchphrase);
        localStorage.setItem("difficulty", difficulty);
        if (this.image != null) {
            localStorage.setItem("pfp", this.image);
        }
    }

    /** Handle input images - checking type and count */
    handleFiles(files) {
        if (files.length > 1) {
            console.log("TOO MANY PICS");
            return;
        } else if (files.length != 1 || !files[0].type.includes("image")) {
            return;
        }
        this.loadImageFromFile(files[0]);
    }

    /** Loading image from file, displaying preview and saving blob */
    loadImageFromFile(file) {
        const pfpDiv = document.querySelector('#pfp');
        const fr = new FileReader();
        fr.addEventListener("load", e => {
            const i = new Image();
            i.addEventListener("load", e=> {
                i.width = window.innerHeight/20;
            })
            if (fr.result != null) {
                pfpDiv.innerHTML = '';
                i.src = fr.result;
                pfpDiv.appendChild(i);
                this.image = fr.result;
            }

        })
        fr.readAsDataURL(file);

    }

    /** Render content of OPTIONS, form includes:
     *  nickname input (mandatory, at least 3 characters), catchphrase input,
     *  profile picture input, difficulty selection.
     *  Difficulty settings change time for gameplay.
     */
    render() {
        return `
            <h2>Options</h2>
            <form id="inputForm">
                <label for="inputName">Nickname:</label><br>
                <input type="text"
                    id="inputName"
                    placeholder="Enter Nickname"
                    required minlength="3"
                    required>
                <br>
                <fieldset class="pictureFieldset">
                    <legend>Optional - drag and drop image or select it:</legend>
                    <div id="inputDivDrop">
                        <label for="inputPfp">Choose a profile picture:</label>
                        <input type="file"
                            id="inputPfp" name="inputPfp"
                            accept="image/png, image/jpeg">
                        <br>
                        <div id="pfp"></div>
                    </div>
                </fieldset>
                <br>
                <label for="inputCatchphrase">User catchphrase:</label><br>
                <input type="text"
                    id="inputCatchphrase"
                    placeholder="For the win!"> 
                <br>
                <fieldset>
                    <legend>Selecting difficulty:</legend>
                    <label>Options:</label><br>
                    <input type="radio" id="inputDifEasy" name="difficulty" required>
                    <label for="inputDifEasy">Easy</label>
                    <input type="radio" id="inputDifNormal" name="difficulty" required checked="true">
                    <label for="inputDifNormal">Normal</label>
                    <input type="radio" id="inputDifHard" name="difficulty" required>
                    <label for="inputDifHard">Hard</label>
                </fieldset>
                <br>
                <input type="submit"> 
            </form>
            `;
    }
}

/** SCOREBOARD Page to show top scores */
export class ScoreboardPage extends Page {
    constructor (settings) {
        super(settings);
    } 

    pageShow() {
        this.pageElement.innerHTML = this.render();
        this.renderTable();
        document.title = this.title;
    }

    renderTable() {
        //init table in local storage if not already done
        if (localStorage.getItem('init') != "true") {
            localStorage.setItem('init', "true");
            for (let i = 0; i < 10; i++) {
                localStorage.setItem(i, 0);
                localStorage.setItem(`${i}nick`, "place not taken");
            }
        }
        const tableEl = document.querySelector('table');
        for (let i = 0; i < 10; i++) {
            const rowEl = document.createElement('tr');
            const numEl = document.createElement('td');
            numEl.innerHTML = i+1;
            const scoreEl = document.createElement('td');
            scoreEl.innerHTML = localStorage.getItem(i);
            const nickEl = document.createElement('td');
            nickEl.innerHTML = localStorage.getItem(`${i}nick`);
            rowEl.append(numEl, scoreEl, nickEl);
            tableEl.appendChild(rowEl);
        }
    }

    render() {
        return `
            <h2>ScoreBoard</h2>
            <table>
                <tr>
                    <th>Place</th>
                    <th>Score</th>
                    <th>Nickname</th>
                </tr>
            </table>
            `;
    }
}

/** TUTORIAL page */
export class TutorialPage extends Page {
    constructor (settings) {
        super(settings);
    } 

    render() {
        return `
            <h2>Tutorial</h2>
            <p>First, set up your profile in options! <br>
            Then, you're ready to go - PLAY! <br>
            Your job is to destroy as many stones as you can within a given time range by placing three or more of the same type in a row or a column. <br>
            Try to swipe two neighboring stones and see what happens! It's quite easy, you'll have it in hand in no time. <br>
            Good luck!!</p>
            `;
    }
}

/** Joke credits page */
export class CreditsPage extends Page {
    constructor (settings) {
        super(settings);
    } 

    render() {
        return `
            <h2>Credits</h2>
            <p>Immersive next gen graphics: me <br>
            Staggering AAA gameplay: me <br>
            Nonexistant music: me <br>
            By: me <br>
            All thanks to: me <br><br>
            ... and hours sitting on html/css/js <br>
            documentation because <br>
            i did not absolve ZWA.</p>
            `;
    }
}

/** Page for link error */
export class PageNotFound extends Page {
    constructor (settings) {
        super(settings);
    }

    render() {
        return `
            <h2>Not found</h2>
            <p>Unknown page</p>
            `;
    }
}

