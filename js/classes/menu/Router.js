import {media} from '../../Modules.js';
/** Class handling page routing */
export class Router {
    //needs all possible pages in array and one default for start
    constructor({pages, defaultPage}) { 
        this.pages = pages;
        this.defaultPage = defaultPage;
        this.currentPage = null;
        this.button = document.querySelector('#backbutton');

        //reacting to historyAPI going back
        window.addEventListener('popstate', e => {
            this.route(window.location.href);
            const url = new URL(window.location.href);
            const page = url.searchParams.get('page');
            if (page == null) {
                document.body.classList.remove('innerpage-visible');
            } else {
                document.body.classList.add('innerpage-visible');
            }
        });
        //reacting to user link click
        window.addEventListener('click', e => {	
            const element = e.target;
            //routing to inner page within this page
            if (element.className === 'innerpage') {
                e.preventDefault();
                this.route(element.href);
                //add page to history
                window.history.pushState(null, null, element.href);
                //play click sound
                media.clickSound.pause();
                media.clickSound.currentTime = 0;
                media.clickSound.play();
            }
            //routing to different page - gamepage, timeout for click sound to be heard
            if (element.className === 'gamepage') {
                e.preventDefault();
                //play click sound
                media.clickSound.pause();
                media.clickSound.currentTime = 0;
                media.clickSound.play();
                setTimeout(function() { window.location = element.href; }, 500);
            }
        });
        //backbutton click -> hide content page
        this.button.addEventListener('click', (e) => {
            if (this.currentPage) {
                this.currentPage.pageHide();
            }
            this.currentPage = this.pages.find(p => p.key == this.defaultPage);
            this.currentPage.pageShow();
            document.body.classList.remove('innerpage-visible');
            //add no-page-showing state to history
            window.history.pushState(null, null, document.location.origin);
            //play click sound
            media.clickSound.pause();
            media.clickSound.currentTime = 0;
            media.clickSound.play();
        });
    }

    /** Render and show targeted innerpage */
    route(urlString) {
        //get page code
        const url = new URL(urlString);
        const page = url.searchParams.get('page');
        //hide current page if one is shown
        if (this.currentPage) {
            this.currentPage.pageHide();
        }
        //find correct page according to page code and render/show it
        const page404 = this.pages.find(p => p.key == '404');
        const pageInstanceMatched = this.pages.find(p => p.key === (page ?? this.defaultPage));
        const currentPage = pageInstanceMatched ?? page404;
        this.currentPage = currentPage;
        this.currentPage.pageShow();
        document.body.classList.add('innerpage-visible');
    }
}